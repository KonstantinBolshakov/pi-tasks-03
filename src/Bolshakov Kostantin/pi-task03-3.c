#define N 256
#include <stdio.h>
#include <string.h>


int main()
 {
	char word[N];
	int count[N] = { 0 };
	int i = 0;
	
		printf("Please, enter word:\n");
	fgets(word, 256, stdin);
	word[strlen(word) - 1] = 0;
	printf("Letter frequency:\n");
		do 
		{
	    	count[word[i++]]++;
		
	     } 
		while (word[i]);
		 
			for (i = 0; i<N; i++)
			 {
				if (count[i] != 0)
					
					printf("%c = %d \n", i, count[i]);
			}
		return 0;
 }